import axios from 'axios';
import bcrypt from "bcrypt";
import bodyParser from "body-parser";
import cors from "cors";
import express, { NextFunction } from "express";
import http from "http";
import jwt from "jsonwebtoken";
import multer from "multer";
import cron from 'node-cron';
import path from "path";
import { Socket, Server as SocketIOServer } from "socket.io";
import swaggerUi from "swagger-ui-express";
import { setupDB } from "./middlewares/db";
import swaggerDocument from "./swagger.json";
// ... Other imports ...

// Set up multer storage and limits
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });
const coloredLoggerMiddleware = require("./middlewares/logs");
const secretKey = "votacao-app-camara-secret";
const app = express();
const server = http.createServer(app);
const io: any = new SocketIOServer(server);
const port = process.env.PORT || 3333;
const db = setupDB();

// Define a pasta de build da aplicação React
const buildPath = path.join(__dirname, "dist");

function verifyToken(req: any, res: any, next: NextFunction) {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({ error: "No token provided" });
  }

  jwt.verify(
    token.replace("Bearer ", ""),
    secretKey,
    (err: any, decoded: any) => {
      if (err) {
        console.log(err);
        return res.status(401).json({ error: "Invalid token" });
      }
      req.userId = decoded.userId;
      next();
    }
  );
}

// Serve os arquivos estáticos da pasta de build
app.use(express.static(buildPath));

app.get("/users/:id/image", (req, res) => {
  const userId = req.params.id;

  db.get("SELECT image FROM users WHERE id = ?", [userId], (err, row: any) => {
    if (err) {
      return res.status(500).json({ error: "Error fetching user image" });
    }

    if (!row || !row.image) {
      return res.status(404).json({ error: "User image not found" });
    }

    // Send the image as a response
    res.end(row.image);
  });
});

// Rota para servir o build
app.get("/app/*", (req, res) => {
  res.sendFile(path.join(buildPath, "index.html"));
});

app.use(cors());
app.use(bodyParser.json());
app.use(coloredLoggerMiddleware);

io.on("connection", (socket: Socket) => {
  console.log("Novo cliente conectado:", socket.id);

  socket.on("disconnect", () => {
    console.log("A user disconnected");
  });
});

// Emit voting results to connected clients
function emitVotacaoResults(votingId: string) {
  db.all(
    "SELECT votos.*, users.username, users.email, users.partido, users.cargo, users.image FROM votos JOIN users ON votos.id_user = users.id WHERE id_votacoes = ?",
    [votingId],
    (err, row) => {
      if (err) {
        return console.log({ error: "Error fetching vote" });
      }

      if (!row || row.length === 0) {
        return console.log({ error: "No votes found" });
      }

      console.log("votacaoResults_" + votingId, row);
      io.emit("votacaoResults_" + votingId, row);
    }
  );
}

// Route to subscribe to votacao results and emit updates
app.post("/subscribe-votacao/:id", (req, res) => {
  const votingId = req.params.id;

  // Emit initial results to the client upon subscription
  emitVotacaoResults(votingId);

  // Client is now subscribed to updates for this votacao
  res.json({ message: "Subscribed to votacao results" });
});

// Simulate voting and update results (for demonstration purposes)
app.post("/vote/:idUser/:id/:voteType", verifyToken, (req, res) => {
  const votingId = req.params.id;
  const idUser = req.params.idUser;
  const voteType = req.params.voteType;

  // Update votacao results in the database
  db.run(
    `UPDATE votacoes SET total_${voteType} = total_${voteType} + 1, total_votos = total_votos + 1 WHERE id = ?`,
    [votingId],
    function (err) {
      if (err) {
        console.error(err);
        return res
          .status(500)
          .json({ error: "Error updating votacao results" });
      }

      // Insert the vote into the votos table
      db.run(
        "INSERT INTO votos (voto_sim, voto_nao, voto_abs, id_votacoes, id_user) VALUES (?, ?, ?, ?, ?)",
        [
          voteType === "sim" ? 1 : 0,
          voteType === "nao" ? 1 : 0,
          voteType === "abs" ? 1 : 0,
          votingId,
          idUser,
        ],
        function (err) {
          if (err) {
            console.error(err);
            return res.status(500).json({ error: "Error recording vote" });
          }

          // Emit updated results to subscribed clients
          emitVotacaoResults(votingId);

          res.json({ message: "Vote recorded successfully" });
        }
      );
    }
  );
});

app.get("/votes/:idUser/:idVotacoes", verifyToken, (req, res) => {
  const idUser = req.params.idUser;
  const idVotacoes = req.params.idVotacoes;

  db.get(
    "SELECT * FROM votos WHERE id_user = ? AND id_votacoes = ?",
    [idUser, idVotacoes],
    (err, row) => {
      if (err) {
        return res.status(500).json({ error: "Error fetching vote" });
      }

      if (!row) {
        return res.status(404).json({ error: "Vote not found" });
      }

      res.json(row);
    }
  );
});

app.get("/votes/:idVotacoes", verifyToken, (req, res) => {
  const idVotacoes = req.params.idVotacoes;

  db.all(
    "SELECT votos.*, users.username, users.email, users.partido, users.cargo, users.image FROM votos JOIN users ON votos.id_user = users.id WHERE id_votacoes = ?",
    [idVotacoes],
    (err, row) => {
      if (err) {
        return res.status(500).json({ error: "Error fetching vote" });
      }

      if (!row || row.length === 0) {
        return res.status(404).json({ error: "No votes found" });
      }

      res.json(row);
    }
  );
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Create a new votacao
app.post("/votacoes", verifyToken, (req, res) => {
  const { titulo, status, texto, indicacao } = req.body;

  console.log("Criando sessao de votacao!!: ", {
    status: status,
    texto: texto,
    indicacao,
  });

  db.run(
    "INSERT INTO votacoes (total_sim, total_nao, total_votos, total_abs, titulo, status, texto, indicacao) VALUES (0, 0, 0, 0, ?, ?, ?, ?)",
    [titulo, status, texto, indicacao],
    function (err) {
      if (err) {
        return res.status(500).json({ error: "Error creating votacao" });
      }

      res.json({ id: this.lastID });
    }
  );
});

app.put("/votacao/:id", verifyToken, (req, res) => {
  const { id } = req.params;

  db.run("UPDATE votacoes SET ativo = 0 WHERE id = ?", [id], function (err) {
    if (err) {
      return res.status(500).json({ error: "Error changing session status" });
    }

    res.json({ message: `Session with ID ${id} set as inactive` });
  });
});

// Get the last votacao
app.get("/votacao/:id", verifyToken, (req, res) => {
  const { id } = req.params;

  db.get("SELECT * FROM votacoes WHERE id=?", [id], (err, rows) => {
    if (err) {
      return res.status(500).json({ error: "Error fetching active votacoes" });
    }

    if (!rows) {
      return res.status(404).json({ error: "No active votacoes found" });
    }

    res.json(rows);
  });
});

app.get("/ultima-votacao", verifyToken, (req, res) => {
  db.get("SELECT * FROM votacoes WHERE ativo = 1 ORDER BY id DESC LIMIT 1", (err, row) => {
    if (err) {
      return res.status(500).json({ error: "Error fetching the latest votacao" });
    }

    if (!row) {
      return res.status(404).json({ error: "No votacoes found" });
    }

    res.json(row);
  });
});


// Get the last votacao
app.get("/last-votacao", verifyToken, (req, res) => {
  db.all(
    "SELECT * FROM votacoes WHERE ativo = 1 ORDER BY id DESC",
    (err, rows) => {
      if (err) {
        return res
          .status(500)
          .json({ error: "Error fetching active votacoes" });
      }

      if (!rows || rows.length === 0) {
        return res.status(404).json({ error: "No active votacoes found" });
      }

      res.json(rows);
    }
  );
});

app.post("/token", (req, res) => {
  const { email, password } = req.body;

  // Authenticate the user and generate a token
  db.get(
    "SELECT * FROM users WHERE email = ?",
    [email],
    async (err, row: any) => {
      if (err) {
        return res.status(500).json({ error: "Error authenticating user" });
      }

      if (!row) {
        return res.status(401).json({ error: "Authentication failed" });
      }

      const passwordMatch = await bcrypt.compare(password, row.password);

      if (!passwordMatch) {
        return res.status(401).json({ error: "Authentication failed" });
      }

      // Generate a token with user ID as payload
      const token = jwt.sign({ userId: row.id }, secretKey, {
        expiresIn: "1h",
      });

      res.json({ token });
    }
  );
});

// Authenticate user
app.post("/auth", async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({ error: "Email and password are required" });
  }

  // Check if user exists in the admins table
  db.get(
    "SELECT * FROM admins WHERE email = ?",
    [email], // Assuming 'email' is the admin's username
    async (err, adminRow: any) => {
      if (err) {
        return res.status(500).json({ error: "Error authenticating user" });
      }

      if (adminRow) {
        // If the user is an admin, compare passwords
        const passwordMatch = await bcrypt.compare(password, adminRow.password);

        if (passwordMatch) {
          // Generate a token for admin
          const token = jwt.sign(
            {
              id: adminRow.id,
              username: adminRow.username,
              email: adminRow.email,
              admin: "true",
            },
            secretKey,
            { expiresIn: "1h" }
          );

          return res.json({
            message: "Admin authentication successful",
            token,
            admin: "true",
          });
        }
      }

      // If the user is not an admin or the password doesn't match, check in the users table
      db.get(
        "SELECT * FROM users WHERE email = ?",
        [email],
        async (err, userRow: any) => {
          if (err) {
            return res.status(500).json({ error: "Error authenticating user" });
          }

          if (!userRow) {
            return res.status(401).json({ error: "Authentication failed" });
          }

          const passwordMatch = await bcrypt.compare(
            password,
            userRow.password
          );

          if (!passwordMatch) {
            return res.status(401).json({ error: "Authentication failed" });
          }

          res.json({
            message: "User authentication successful",
            token: jwt.sign(
              {
                id: userRow.id,
                username: userRow.username,
                email: userRow.email,
                cargo: userRow.cargo,
                partido: userRow.partido,
                admin: "false",
              },
              secretKey,
              { expiresIn: "1h" }
            ),
            admin: "false",
          });
        }
      );
    }
  );
});

// Rota para criar um administrador
app.post("/create-admin", async (req, res) => {
  const { email, password } = req.body;

  console.log({ email, password });
  if (!email || !password) {
    return res.status(400).json({ error: "Email and password are required" });
  }

  try {
    // Criptografe a senha antes de armazená-la no banco de dados
    const hashedPassword = await bcrypt.hash(password, 10);

    // Insira o novo administrador na tabela
    db.run(
      "INSERT INTO admins (email, password, admin) VALUES (?, ?, true)",
      [email, hashedPassword],
      function (err) {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: "Error creating admin" });
        }
        res.json({
          message: "Admin created successfully",
          adminId: this.lastID,
        });
      }
    );
  } catch (error) {
    console.error("Error hashing password:", error);
    res.status(500).json({ error: "Error creating admin" });
  }
});

// Create a new user with image and additional fields
app.post(
  "/users",
  upload.fields([{ name: "image" }]),
  async (req: any, res) => {
    console.log(req.body);
    const { username, password, email, partido, cargo } = req.body;

    if (!username || !password || !email) {
      return res
        .status(400)
        .json({ error: "Username, password, and email are required" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    let imageBuffer = null;
    if (req.files && req.files["image"] && req.files["image"][0]) {
      imageBuffer = req.files["image"][0].buffer;
    }

    db.run(
      "INSERT INTO users (username, password, email, image, partido, cargo) VALUES (?, ?, ?, ?, ?, ?)",
      [username, hashedPassword, email, imageBuffer, partido, cargo],
      function (err) {
        if (err) {
          return res.status(500).json({ error: "Error creating user" });
        }
        res.json({ id: this.lastID });
      }
    );
  }
);

app.get("/users/:id", verifyToken, (req, res) => {
  const userId = req.params.id;

  db.get(
    "SELECT id, username, email FROM users WHERE id = ?",
    [userId],
    (err, row) => {
      if (err) {
        return res.status(500).json({ error: "Error fetching user" });
      }

      if (!row) {
        return res.status(404).json({ error: "User not found" });
      }

      res.json(row);
    }
  );
});

app.get("/users", verifyToken, (req, res) => {
  db.all("SELECT id, username, email, image FROM users", [], (err, rows) => {
    if (err) {
      return res.status(500).json({ error: "Error fetching users" });
    }

    if (!rows || rows.length === 0) {
      return res.status(404).json({ error: "No users found" });
    }

    console.log(rows);

    res.json(rows);
  });
});

// Update a user
app.put("/users/:id", verifyToken, async (req, res) => {
  const userId = req.params.id;
  const { username, password, email } = req.body;

  console.log(username, password, email);

  const hashedPassword = await bcrypt.hash(password, 10);

  db.run(
    "UPDATE users SET username = ?, password = ?, email = ? WHERE id = ?",
    [username, hashedPassword, email, userId],
    function (err) {
      if (err) {
        return res.status(500).json({ error: "Error updating user" });
      }
      res.json({ message: "User updated successfully" });
    }
  );
});

// Delete a user
app.delete("/users/:id", verifyToken, (req, res) => {
  const userId = req.params.id;

  db.run("DELETE FROM users WHERE id = ?", [userId], function (err) {
    if (err) {
      return res.status(500).json({ error: "Error deleting user" });
    }
    res.json({ message: "User deleted successfully" });
  });
});

const fetchPage = async () => {
  const urlToFetch = "https://projeto-camara.onrender.com/app/"
  try {
    const response = await axios.get(urlToFetch);
    console.log(`Fetched ${urlToFetch} - Status: ${response.status}`);
    // You can process the content of the response here if needed
  } catch (error: any) {
    console.error(`Error fetching ${urlToFetch}: ${error.message}`);
  }
};

// Schedule the cron job to run every minute
cron.schedule('*/10 * * * *', fetchPage); // You can adjust the cron schedule as needed

console.log('Cron job scheduled to fetch the page.');

// Start the server
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
  fetchPage()
});
