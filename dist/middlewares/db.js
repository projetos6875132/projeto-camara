"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupDB = void 0;
const sqlite3_1 = __importDefault(require("sqlite3"));
const setupDB = () => {
    const db = new sqlite3_1.default.Database("./database.db");
    // Crie a tabela de administradores se ela não existir
    db.run(`
CREATE TABLE IF NOT EXISTS admins (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  email TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL,
  admin  INTEGER NOT NULL
)
`);
    // Create users table
    db.run(`
CREATE TABLE IF NOT EXISTS users (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT,
  password TEXT,
  partido TEXT,
  cargo TEXT,
  email TEXT UNIQUE,
  image BLOB
)
`);
    // Create votacoes table if not exists
    db.run(`
  CREATE TABLE IF NOT EXISTS votacoes (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    titulo TEXT NOT NULL,
    status TEXT NOT NULL,
    texto TEXT NOT NULL,
    indicacao TEXT NOT NULL,
    total_sim INTEGER NOT NULL,
    total_nao INTEGER NOT NULL,
    total_abs INTEGER NOT NULL,
    total_votos INTEGER NOT NULL,
    ativo BOOLEAN DEFAULT 1, -- Added "ativo" field
    data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  );
`);
    // Create votos table if not exists
    db.run(`
CREATE TABLE IF NOT EXISTS votos (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  voto_sim INTEGER,
  voto_nao INTEGER,
  voto_abs INTEGER,
  id_votacoes INTEGER,
  id_user INTEGER,
  FOREIGN KEY (id_votacoes) REFERENCES votacoes(id),
  FOREIGN KEY (id_user) REFERENCES users(id)
)
`);
    return db;
};
exports.setupDB = setupDB;
