"use strict";
function coloredLoggerMiddleware(req, res, next) {
    const timestamp = new Date().toISOString();
    const method = req.method;
    const url = req.url;
    // Middleware para logs coloridos
    console.log(`[${timestamp}] ${method} ${url} ${req.ip} - ${req.get("User-Agent")}`);
    next();
}
module.exports = coloredLoggerMiddleware;
